# Data Sharing

The data corresponds to the results in section 3 of the paper:
> Hao Peng, and Xiaoli Bai, “Gaussian Processes for improving orbit prediction accuracy”, Acta Astronautica, vol. 161, Aug. 2019, pp. 44–56. [[Link]](https://linkinghub.elsevier.com/retrieve/pii/S0094576518320344).


All the data are not standardized yet.

| Variable Names  | Descriptions |
| --------------- | ------ |
| `Xs_training`   | N sets of inputs for training. Size: N x 51. |
|                 | index 1: $\Delta T$ in [seconds] |
|                 | index 2--11: $\delta t$, $\delta PV$ (position velocity vector), $C_{d,old}$, $C_{d,current}$, $\delta C_d$ in [m], [m/s], or [1].
|                 | index 12--21: similar to above, for the second pair |
|                 | index 22--31, 32--41, 42--51: similar |
| `Ys_training`   | Ouputs for training. Size: N x 6 |
|                 | Each index corresponds to one components in $\{ e_x, e_y, e_z, e_{vx}, e_{vy}, e_{vz} \}$, in [m] or [m/s]. |
|                 | Some of them should be correlated, but in the paper we've trained 6 separate GPs for each component. |
|                 | |
| `Xs_validation` | Inputs for validation. Same structure. |
| `Ys_validation` | Ouputs for validation. Same structure. |
|                 | |
| `Xs_testing`    | Inputs for testing. Same structure. |
| `Ys_testing`    | Ouputs for testing. Same structure. |

